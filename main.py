# ASH savings and charts
#
#   Data analysis algorithm for anti-sweat heaters (ASH)
#   in supermarket refrigeration technique.
#
#   Dew point data is obtained from ARSO http://www.arso.gov.si/
#   Calculation script predicts 30 min (0.5 h) measurement period
#
#   In order to obtain total savings, power consumption and charts,
#   run "python main.py" using Python 2.7
#   All relevant output is stored in "./output"
#
#   For further reading view the corresponding publication:
#    Optimizing power usage of anti-sweat heaters in
#    glass door refrigerators according to dew point
#
#    *link coming soon
#
#   Developed at the Faculty of Electrical Engineering, University of Ljubljana


import calculate as calc
import draw as draw


def main():

    #file_arso = "in_arso_2016.10.7-2016.11.7.csv"
    file_arso = "in_arso_2018.05.01-2018.05.31.csv"
    calc.generate_output(file_arso)

    #file_arso = "out_arso_2016.10.7-2016.11.7.csv"
    file_arso = "out_arso_2018.05.01-2018.05.31.csv"
    file_defog_ok = "defog_ok.csv"
    file_defog_fail = "defog_fail.csv"
    file_measurements = "measurements_oct_2016.csv"
    draw.draw_charts(file_arso, file_defog_ok, file_defog_fail, file_measurements)



if __name__ == "__main__":
    main()
