# ASH savings and charts
#
#   Data analysis algorithm for anti-sweat heaters (ASH)
#   in supermarket refrigeration technique.
#
#   Dew point data is obtained from ARSO http://www.arso.gov.si/
#   Calculation script predicts 30 min (0.5 h) measurement period
#
#   In order to obtain total savings, power consumption and charts,
#   run "python main.py" using Python 2.7
#   All relevant output is stored in "./output"
#
#   For further reading view the corresponding publication:
#    Optimizing power usage of anti-sweat heaters in
#    glass door refrigerators according to dew point
#
#    *link coming soon
#
#   Developed at the Faculty of Electrical Engineering, University of Ljubljana


import os, csv
import math
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import calculate as calc



# -- main drawing funciton
def draw_charts(file_arso, file_defog_ok, file_defog_fail, file_measurements):

    print "\n\t*Started drawing charts..."

    # -- global operations -----------------------------------------------------

    # -- font family
    mpl.rc('font',family='serif')           # -- default font
    mpl.rcParams['mathtext.fontset'] = 'dejavuserif'    # -- font label
    # -- font size
    tickSize = 20
    legendSize = 24
    labelSize = 28

    # -- colors
    _red = "#ff4d4d"
    _green = "#00b33c"
    _blue = "#006bb3"
    _orange = "#ff751a"
    _med_grey = "#dddddd"
    _dark_grey = "#222222"

    # -- figure (subplot) padding
    padd_left = 0.165
    padd_right = 0.835
    padd_bottom = 0.18
    padd_top = 0.99

    # -- label padding
    x_label_padd = 50
    y_label_padd = 110

    # -- CHART #1 --------------------------------------------------------------
    # -- display defog measurements on power vs dew point chart

    # -- save data from file to list
    points_ok_x = []
    points_ok_y = []
    points_ok_z = []
    points_fail_x = []
    points_fail_y = []
    points_fail_z = []

    with open(os.path.join("input",file_defog_ok), 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            points_ok_y.append(float(row[0]))
            points_ok_x.append(float(row[1]))
            points_ok_z.append(int(row[2]))

    with open(os.path.join("input",file_defog_fail), 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            points_fail_y.append(float(row[0]))
            points_fail_x.append(float(row[1]))
            points_fail_z.append(int(row[2]))

    # -- create figure
    fig = plt.figure(figsize = (18,10), dpi = 100);
    plt.subplots_adjust(left = padd_left, bottom = padd_bottom,
        right = padd_right, top = padd_top, wspace = None, hspace = None)
    ax1 = fig.add_subplot(111)

    # -- limits
    ax1.set_xlim(-3, 20)
    ax1.set_ylim(-1, 56)

    # -- ticks
    x_ticks = x_tickLabels = np.arange(-2.5,21,2.5)
    y_ticks = y_tickLabels = np.arange(0,56,5)

    ax1.set_xticks(x_ticks)
    ax1.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.set_yticks(y_ticks)
    ax1.set_yticklabels(y_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.tick_params(axis = "both", rotation = 0, pad = 10)


    # -- labels
    ax1.set_xlabel(r'$T_{\rm DP} \rm\ [^\circ C]$', rotation = 0,
        labelpad = x_label_padd, fontsize = labelSize, family='serif')
    ax1.set_ylabel(r'$P_{\rm ASH} \rm\ [W]$', rotation = 0,
        labelpad = y_label_padd, fontsize = labelSize, family='serif')

    # -- grid
    ax1.grid(which = "major", axis = "both", linewidth = 1, zorder = 2)

    # -- plot the data
    # -- include color profile
    included_times = []
    for i in range(0,len(points_ok_x)):
        r_comp = round((points_ok_z[i] - 70) * 2 * 0.01, 2)
        g_comp = round((1 - 1.65 * r_comp) * 0.75, 2)
        b_comp = round(1 - r_comp, 2)
        #print (r_comp, g_comp, b_comp)
        # -- for gradient legend
        """
        if not(i%5):
            _label = str(points_ok_z[i]) + " s"
        else:
            _label = "_nolegend_"
        """
        if not(points_ok_z[i] in included_times):
            _label = str(points_ok_z[i]) + " s"
            included_times.append(points_ok_z[i])
        else:
            _label = "_nolegend_"
        #_label = str(points_ok_z[i]) + " s"
        ax1.scatter(points_ok_x[i], points_ok_y[i], facecolors='none', s = 80, linewidth = 1.5,
            edgecolors = (r_comp, g_comp, b_comp), zorder = 3.5, label=_label)
    for i in range(0,len(points_fail_x)):
        r_comp = (points_fail_z[i] - 70) * 2 * 0.01
        r_comp = round(5 ** r_comp / 5, 2)
        g_comp = round(r_comp * 0.15, 2)
        b_comp = round(1 - r_comp,2)
        """
        # -- for gradient legend
        if not((i)%2):
            _label = str(points_fail_z[i]) + " s"
        else:
            _label = "_nolegend_"
        """
        if not(points_fail_z[i] in included_times):
            _label = str(points_fail_z[i]) + " s"
            included_times.append(points_fail_z[i])
        else:
            _label = "_nolegend_"
        #_label = str(points_fail_z[i]) + " s"
        #ax1.scatter(points_fail_x[i], points_fail_y[i], marker = 'x', s = 80,
        ax1.scatter(points_fail_x[i], points_fail_y[i], facecolors='none', s = 80, linewidth = 1.5,
            edgecolors = (r_comp, g_comp, b_comp), zorder = 3.5, label = _label)

    #ax1.scatter(points_ok_x, points_ok_y, facecolors='none', s = 80, linewidth = 1.5,
    #    edgecolors = _green, zorder = 3.5, label = "Defog time <= 100 s")
    #ax1.scatter(points_fail_x, points_fail_y, marker = 'x', s = 80,
    #    color = _red, zorder = 3.5, label = "Defog time > 100 s")

    # -- legend
    ax1.legend(loc = "upper right", fontsize = legendSize, ncol=1)

    # -- save chart
    fig.savefig(
        os.path.join("output",'figure-1.tiff'), transparent = False)


    # -- CHART #2 --------------------------------------------------------------
    # -- display defog measurements on power vs dew point chart
    # -- + ASH power vs. DP dependencies

    # -- create figure
    fig = plt.figure(figsize = (18,10), dpi = 100);
    plt.subplots_adjust(left = padd_left, bottom = padd_bottom,
        right = padd_right, top = padd_top, wspace = None, hspace = None)
    ax1 = fig.add_subplot(111)

    # -- limits
    ax1.set_xlim(-3, 20)
    ax1.set_ylim(-1, 56)

    # -- ticks
    x_ticks = x_tickLabels = np.arange(-2.5,21,2.5)
    y_ticks = y_tickLabels = np.arange(0,56,5)

    ax1.set_xticks(x_ticks)
    ax1.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.set_yticks(y_ticks)
    ax1.set_yticklabels(y_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.tick_params(axis = "both", rotation = 0, pad = 10)


    # -- labels
    ax1.set_xlabel(r'$T_{\rm DP} \rm\ [^\circ C]$', rotation = 0,
        labelpad = x_label_padd, fontsize = labelSize, family='serif')
    ax1.set_ylabel(r'$P_{\rm ASH} \rm\ [W]$', rotation = 0,
        labelpad = y_label_padd, fontsize = labelSize, family='serif')

    # -- grid
    ax1.grid(which = "major", axis = "both", linewidth = 1, zorder = 2)

    # -- door frame heater
    x_exp1 = np.linspace(-3.59,4.82,100)
    x_exp2 = np.linspace(4.82,8.645,100)
    y_exp1 = [(0.04 * DP**2 + 0.32 * DP + 0.61) for DP in x_exp1]
    y_exp2 =[(1.06 * DP**2 - 0.75 * DP - 17.8) for DP in x_exp2]

    x_exp = np.concatenate([x_exp1, x_exp2])
    y_exp = np.concatenate([y_exp1, y_exp2])

    # -- glass surface heater
    x_lin = np.linspace(-2.211,21,100)
    #y_lin = [(1.61 * DP + 3.56) for DP in x_lin]
    y_lin = [(1.11 * DP + 2.23) for DP in x_lin]

    # -- left border for drawing "OK region"
    left_border = [-5] * 200

    # -- plot
    ax1.fill_betweenx(y_exp, x_exp, left_border,
        facecolor = _med_grey, zorder = 1)
    ax1.plot(x_exp, y_exp, '-', linewidth = 2.5, markersize = 5,
        color = _blue, label = "Door frame ASH", zorder = 4)
    ax1.plot(x_lin, y_lin, '-', linewidth = 2.5, markersize = 5,
        color = _orange, label = "Glass surface ASH", zorder = 4 )
    ax1.scatter(points_ok_x, points_ok_y, facecolors='none', s = 80, linewidth = 1.5,
        edgecolors = _green, zorder = 3.5, label = "Defog time <= 100 s")
    ax1.scatter(points_fail_x, points_fail_y, marker = 'x', s = 80,
        color = _red, zorder = 3.5, label = "Defog time > 100 s")

    # -- legend
    ax1.legend(loc = "upper right", fontsize = legendSize)

    # -- save chart
    fig.savefig(
        os.path.join("output",'figure-2.tiff'), transparent = False)


    # -- CHART #3 --------------------------------------------------------------
    # -- outside (ARSO) and inside (measurements) dew point: 23.10 - 26.10.2016

    dp_out = []
    dp_in = []

    # -- import inside (measurements) and outside (ARSO) dew point data
    with open(os.path.join("input",file_measurements), 'r') as f_measure:
        reader = csv.reader(f_measure)
        for row in reader:
            # -- skip comment
            if (row[0][0] != '*'):
                dp_out.append(float(row[1]))
                dp_in.append(float(row[2]))

    # -- create figure
    fig = plt.figure(figsize = (18,10), dpi = 100);
    plt.subplots_adjust(left = padd_left, bottom = padd_bottom,
        right = padd_right, top = padd_top, wspace = None, hspace = None)
    ax1 = fig.add_subplot(111)

    # -- limits
    ax1.set_xlim(0, 192)
    ax1.set_ylim(-3, 19)

    # -- ticks
    x_ticks = np.arange(0,192,48)
    x_tickLabels = ["2016-10-23",
                    "2016-10-24",
                    "2016-10-25",
                    "2016-10-26"]
    y_ticks = np.arange(-2,19,2)
    y_tickLabels = y_ticks

    ax1.set_xticks(x_ticks)
    ax1.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.set_yticks(y_ticks)
    ax1.set_yticklabels(y_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.tick_params(axis = "both", rotation = 0, pad = 10)

    # -- labels
    ax1.set_xlabel(r'$Date$', rotation = 0,
        labelpad = x_label_padd, fontsize = labelSize, family = 'serif')
    ax1.set_ylabel(r'$T_{\rm DP} \rm\ [^\circ C]$', rotation = 0,
        labelpad = y_label_padd, fontsize = labelSize, family = 'serif')

    # -- grid
    ax1.grid(which = "major", axis = "both", linewidth = 1, zorder = 2)

    # -- plot the data
    ax1.plot(dp_out, '-', linewidth = 2.5,
        color = _blue, label = "Outside dew point",  zorder = 4)
    ax1.plot(dp_in, '-', linewidth=2.5,
        color = _orange, label = "Inside dew point",  zorder = 4)

    # -- legend
    ax1.legend(loc = "upper right", fontsize = legendSize)

    # -- save chart
    fig.savefig(
        os.path.join("output",'figure-3.tiff'), transparent = False)


    # -- in/out dp and energy consumption
    timestamp = []
    power_unopt = []
    power_exp = []
    power_lin = []
    dp_in = []
    dp_out = []
    energy_unopt = 0
    energy_exp = 0
    energy_lin = 0

    # -- calculate power for all thre ASH
    with open(os.path.join("output",file_arso), 'r') as f_output:
        reader = csv.reader(f_output)
        for row in reader:
            # -- skip comment
            if (row[0][0] != '*'):
                timest = row[0]
                date = calc.get_date(timest)
                timestamp.append(date)
                # -- DP as array
                dp_out.append(float(row[3]))
                dp_in.append(float(row[4]))
                # -- momentary power
                power_unopt.append(calc._calc_power_unoptimized(timest))
                # -- calculate power based on current (latest) inside DP
                #power_exp.append(calc._calc_power_exp(dp_in[-1], timest))
                #power_lin.append(calc._calc_power_lin(dp_in[-1], timest))
                # -- get power from output file
                power_exp.append(float(row[6]))
                power_lin.append(float(row[7]))
                # -- energy sum
                energy_unopt += calc._calc_power_unoptimized(timest) * 0.5
                energy_exp += calc._calc_power_exp(dp_in[-1], timest) * 0.5
                energy_lin += calc._calc_power_lin(dp_in[-1], timest) * 0.5

    savings_exp = (energy_unopt - energy_exp) / energy_unopt * 100.0
    savings_lin = (energy_unopt - energy_lin) / energy_unopt * 100.0


    # -- CHART #4 --------------------------------------------------------------
    # -- outside (ARSO) and inside (reconstruction) dew point

    # -- for may 2018
    xData1 = dp_out
    xData2 = dp_in
    timestamp_tmp = timestamp
    #xData1 = dp_out[287:670]
    #xData2 = dp_in[287:670]
    #timestamp_tmp = timestamp[287:670]

    x_rotation = 30
    padd_bottom = padd_bottom + 0.07

    # -- create figure
    fig = plt.figure(figsize = (18,10), dpi = 100);
    plt.subplots_adjust(left = padd_left, bottom = padd_bottom,
        right = padd_right, top = padd_top, wspace = None, hspace = None)
    ax1 = fig.add_subplot(111)

    # -- limits
    #ax1.set_xlim(0, 1536)
    ax1.set_xlim(0, len(xData1)-1)
    ax1.set_ylim(-3, 19)

    # -- ticks
    x_ticks = np.arange(0,len(xData1),144)
    #x_ticks = np.arange(0,len(xData1),48)
    x_tickLabels = timestamp_tmp[::144]
    #x_tickLabels = timestamp_tmp[::48]
    y_ticks = y_tickLabels = np.arange(-2,19,2)

    ax1.set_xticks(x_ticks)
    ax1.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.set_yticks(y_ticks)
    ax1.set_yticklabels(y_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.tick_params(axis = "both", rotation = 0, pad = 10)
    ax1.tick_params(axis = "x", rotation = x_rotation)

    # -- labels
    ax1.set_xlabel(r'$Date$', rotation = 0,
        labelpad = x_label_padd, fontsize = labelSize, family = 'serif')
    ax1.set_ylabel(r'$T_{\rm DP} \rm\ [^\circ C]$', rotation = 0,
        labelpad = y_label_padd, fontsize = labelSize, family = 'serif')

    # -- grid
    ax1.grid(which = "major", axis = "both", linewidth = 1, zorder = 2)


    # -- plot the data
    ax1.plot(xData1, '-', linewidth = 2.5,
        color = _blue, label = "Outside dew point",  zorder = 4)
    ax1.plot(xData2, '-', linewidth=2.5,
        color = _orange, label = "Inside dew point",  zorder = 4)

    # -- legend
    ax1.legend(loc = "upper right", fontsize = legendSize)

    # -- save chart
    fig.savefig(
        os.path.join("output",'figure-4.tiff'), transparent = False)


   # -- CHART #5a, 7 days of data predicted (336 entries, 30 min period)

    # -- for oct/nov 2016
    #xData1 = power_exp[1200:]
    #xData2 = power_lin[1200:]
    #xData3 = power_unopt[1200:]
    #timestamp_tmp = timestamp[1200:]
    # -- for may 2018
    xData1 = power_exp[287:670]
    xData2 = power_lin[287:670]
    xData3 = power_unopt[287:670]
    xData_dp = dp_in[287:670]
    timestamp_tmp = timestamp[287:670]

    x_rotation = 30

    # -- create figure
    fig = plt.figure(figsize = (18,10), dpi = 100);
    plt.subplots_adjust(left = padd_left, bottom = padd_bottom,
    right = padd_right, top = padd_top, wspace = None, hspace = None)
    ax1 = fig.add_subplot(111)

    # -- limits
    """
    ax1.set_xlim(0, 335)
    ax1.set_ylim(-1, 76)
    """
    ax1.set_xlim(0, len(xData1)-1)
    ax1.set_ylim(-1, 76)

    # -- ticks
    """
    x_ticks = np.arange(0,336,167)
    x_tickLabels = timestamp[1200:][::167]                 # -- each third day
    """
    x_ticks = np.arange(0,len(xData1),48)
    x_tickLabels = timestamp_tmp[::48]                 # -- each third day
    y_ticks = np.arange(0,76,5)
    y_tickLabels = y_ticks


    ax1.set_xticks(x_ticks)
    ax1.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.set_yticks(y_ticks)
    ax1.set_yticklabels(y_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.tick_params(axis = "both", rotation = 0, pad = 10)
    ax1.tick_params(axis = "x", rotation = x_rotation)

    # -- labels
    ax1.set_xlabel(r'$Date$', rotation = 0,
        labelpad = x_label_padd, fontsize = labelSize, family = 'serif')
    ax1.set_ylabel(r'$P_{\rm ASH} \rm\ [W]$', rotation = 0,
        labelpad = y_label_padd, fontsize = labelSize, family = 'serif')

    # -- grid
    ax1.grid(which = "major", axis = "both", linewidth = 1, zorder = 2)

    # -- plot the data
    """
    ax1.plot(power_exp[1200:], '-', linewidth = 2.5,
        color = _blue, label = "Door frame ASH",  zorder = 4)
    ax1.plot(power_lin[1200:], '-', linewidth=2.5,
        color = _orange, label = "Glass surface ASH",  zorder = 4)
    ax1.plot(power_unopt[1200:], '--', linewidth=2.5,
        color = _dark_grey, label = "Current state",  zorder = 4)
    """
    ax1.plot(xData1, '-', linewidth = 2.5,
        color = _blue, label = "Door frame ASH",  zorder = 4)
    ax1.plot(xData2, '-', linewidth=2.5,
        color = _orange, label = "Glass surface ASH",  zorder = 4)
    ax1.plot(xData3, '--', linewidth=2.5,
        color = _dark_grey, label = "Current state",  zorder = 4)

    # -- legend
    ax1.legend(loc = "upper right", fontsize = legendSize)

    # -- save chart
    fig.savefig(
    os.path.join("output",'figure-5a.tiff'), transparent = False)



    # -- CHART #5b, same as #5a with added inside dew point

    # -- create figure
    fig = plt.figure(figsize = (18,10), dpi = 100);
    plt.subplots_adjust(left = padd_left, bottom = padd_bottom,
        right = padd_right, top = padd_top, wspace = None, hspace = None)
    ax1 = fig.add_subplot(111)
    # -- define second axis (Tdp)
    ax2 = ax1.twinx()

    # -- limits
    #ax1.set_xlim(0, 335)
    ax1.set_xlim(0, len(xData1)-1)
    ax1.set_ylim(-1, 101)

    # -- ticks
    #x_ticks = np.arange(0,336,167)
    #x_tickLabels = timestamp[1200:][::167]
    y_ticks = np.arange(0,101,5)
    y_tickLabels = y_ticks

    y_tickLabels = ["0", "5", "10", "15", "20", "25", "30", "35", "40", "45",
        "50", "55", "", "", "", "", "", "", "", ""]

    ax1.set_xticks(x_ticks)
    ax1.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.set_yticks(y_ticks)
    ax1.set_yticklabels(y_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.tick_params(axis = "both", rotation = 0, pad = 10)
    ax1.tick_params(axis = "x", rotation = x_rotation)

    # -- labels
    ax1.set_xlabel(r'$Date$', rotation = 0,
        labelpad = x_label_padd, fontsize = labelSize, family = 'serif')
    ax1.set_ylabel(r'$P_{\rm ASH} \rm\ [W]$', rotation = 0,
        labelpad = y_label_padd, fontsize = labelSize, family = 'serif')

    # -- grid
    ax1.grid(which = "major", axis = "both", linewidth = 1, zorder = 2)

    # -- plot the data
    lns1 = ax1.plot(xData1, '-', linewidth = 2.5,
        color = _blue, label = "Door frame ASH",  zorder = 4)
    lns2 = ax1.plot(xData2, '-', linewidth=2.5,
        color = _orange, label = "Glass surface ASH",  zorder = 4)
    lns3 = ax1.plot(xData3, '--', linewidth=2.5,
        color = _dark_grey, label = "Current state",  zorder = 4)

    # -- second axis
    #ax2.set_xlim(0, 335)
    ax1.set_xlim(0, len(xData1)-1)
    ax2.set_ylim(-61, 41)

    y_ticks2 = np.arange(-60, 41,5)
    y_tickLabels2 = y_ticks2

    y_tickLabels2 = ["", "", "", "", "", "", "", "", "", "", "",
        "-5", "0", "10", "15", "", "", "", "", ""]

    ax2.set_xticks(x_ticks)
    ax2.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax2.set_yticks(y_ticks2)
    ax2.set_yticklabels(y_tickLabels2, fontsize = tickSize, family = 'serif')

    ax2.tick_params(axis = "both", rotation = 0, pad = 10)

    ax2.set_ylabel(r'$T_{\rm DP} \rm\ [^\circ C]$', rotation = 0,
        fontsize = labelSize, family = 'serif')
    # -- move second y-axis labe to same position as first (mirrored)
    ax2.yaxis.set_label_coords(1.15, 0.55)

    lns4 = ax2.plot(xData_dp, '-', linewidth=2.5,
        color = "#913D88", label = "Inside dew point",  zorder = 4)

    # -- legend
    lns = lns1 + lns2 + lns3 + lns4
    labs = [l.get_label() for l in lns]
    ax1.legend(lns, labs, loc = "upper right", fontsize = legendSize)

    # -- save chart
    fig.savefig(
        os.path.join("output",'figure-5b.tiff'), transparent = False)

    """

    # -- CHART #5c

    # -- create figure
    fig = plt.figure(figsize = (18,10), dpi = 100);
    plt.subplots_adjust(left = padd_left, bottom = padd_bottom,
       right = padd_right, top = padd_top, wspace = None, hspace = None)
    ax1 = fig.add_subplot(111)

    # -- limits
    ax1.set_xlim(0, 719)
    ax1.set_ylim(-1, 76)

    # -- ticks
    #x_ticks = np.arange(0,719,359)
    #x_ticks = np.arange(0,721,48)
    x_ticks = np.arange(0,721, 144)                 # -- each third day
    #x_tickLabels = timestamp[::359]
    #x_tickLabels = timestamp[::48]
    x_tickLabels = timestamp[::144]                 # -- each third day
    y_ticks = np.arange(0,76,5)
    y_tickLabels = y_ticks

    # -- timestamp[719] -> 21.10.16 | timestamp[720] -> 22.10.16

    ax1.set_xticks(x_ticks)
    ax1.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.set_yticks(y_ticks)
    ax1.set_yticklabels(y_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.tick_params(axis = "both", rotation = 0, pad = 10)
    #ax1.tick_params(axis = "x", rotation = 45, pad = 10)

    # -- labels
    ax1.set_xlabel(r'$Date$', rotation = 0,
       labelpad = x_label_padd, fontsize = labelSize, family = 'serif')
    ax1.set_ylabel(r'$P_{\rm ASH} \rm\ [W]$', rotation = 0,
       labelpad = y_label_padd, fontsize = labelSize, family = 'serif')

    # -- grid
    ax1.grid(which = "major", axis = "both", linewidth = 1, zorder = 2)

    # -- plot the data
    ax1.plot(power_exp[:719], '-', linewidth = 2.5,
       color = _blue, label = "Door frame ASH",  zorder = 4)
    ax1.plot(power_lin[:719], '-', linewidth=2.5,
       color = _orange, label = "Glass surface ASH",  zorder = 4)
    ax1.plot(power_unopt[:719], '--', linewidth=2.5,
       color = _dark_grey, label = "Current state",  zorder = 4)

    # -- legend
    ax1.legend(loc = "upper right", fontsize = legendSize)

    # -- save chart
    fig.savefig(
       os.path.join("output",'figure-5c.tiff'), transparent = False)


    # -- CHART #5d

    # -- create figure
    fig = plt.figure(figsize = (18,10), dpi = 100);
    plt.subplots_adjust(left = padd_left, bottom = padd_bottom,
       right = padd_right, top = padd_top, wspace = None, hspace = None)
    ax1 = fig.add_subplot(111)

    # -- limits
    ax1.set_xlim(0, 335)
    ax1.set_ylim(-1, 76)

    # -- ticks
    x_ticks = np.arange(0,719,359)
    x_tickLabels = timestamp[720:][::359]                 # -- each third day
    y_ticks = np.arange(0,76,5)
    y_tickLabels = y_ticks

    ax1.set_xticks(x_ticks)
    ax1.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.set_yticks(y_ticks)
    ax1.set_yticklabels(y_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.tick_params(axis = "both", rotation = 0, pad = 10)

    # -- labels
    ax1.set_xlabel(r'$Date$', rotation = 0,
       labelpad = x_label_padd, fontsize = labelSize, family = 'serif')
    ax1.set_ylabel(r'$P_{\rm ASH} \rm\ [W]$', rotation = 0,
       labelpad = y_label_padd, fontsize = labelSize, family = 'serif')

    # -- grid
    ax1.grid(which = "major", axis = "both", linewidth = 1, zorder = 2)

    # -- plot the data
    ax1.plot(power_exp[720:1440], '-', linewidth = 2.5,
       color = _blue, label = "Door frame ASH",  zorder = 4)
    ax1.plot(power_lin[720:1440], '-', linewidth=2.5,
       color = _orange, label = "Glass surface ASH",  zorder = 4)
    ax1.plot(power_unopt[720:1440], '--', linewidth=2.5,
       color = _dark_grey, label = "Current state",  zorder = 4)

    # -- legend
    ax1.legend(loc = "upper right", fontsize = legendSize)

    # -- save chart
    fig.savefig(
       os.path.join("output",'figure-5d.tiff'), transparent = False)



    # -- CHART #6 --------------------------------------------------------------
    # -- power comparison for duration of reconstruction

    # -- create figure
    fig = plt.figure(figsize = (18,10), dpi = 100);
    plt.subplots_adjust(left = padd_left, bottom = padd_bottom,
        right = padd_right, top = padd_top, wspace = None, hspace = None)
    ax1 = fig.add_subplot(111)

    # -- limits
    ax1.set_xlim(0, 1536)
    ax1.set_ylim(-1, 76)

    # -- ticks
    x_ticks = np.arange(0,1536,336)
    x_tickLabels = timestamp[::336]                 # -- each third day
    y_ticks = np.arange(0,76,5)
    y_tickLabels = y_ticks

    ax1.set_xticks(x_ticks)
    ax1.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.set_yticks(y_ticks)
    ax1.set_yticklabels(y_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.tick_params(axis = "both", rotation = 0, pad = 10)

    # -- labels
    ax1.set_xlabel(r'$Date$', rotation = 0,
        labelpad = x_label_padd, fontsize = labelSize, family = 'serif')
    ax1.set_ylabel(r'$P_{\rm ASH} \rm\ [W]$', rotation = 0,
        labelpad = y_label_padd, fontsize = labelSize, family = 'serif')

    # -- grid
    ax1.grid(which = "major", axis = "both", linewidth = 1, zorder = 2)

    # -- plot the data
    ax1.plot(power_exp, '-', linewidth = 2.5,
        color = _blue, label = "Door frame ASH",  zorder = 4)
    ax1.plot(power_lin, '-', linewidth=2.5,
        color = _orange, label = "Glass surface ASH",  zorder = 4)
    ax1.plot(power_unopt, '--', linewidth=2.5,
        color = _dark_grey, label = "Current state",  zorder = 4)

    # -- legend
    ax1.legend(loc = "upper right", fontsize = legendSize)

    # -- save chart
    fig.savefig(
        os.path.join("output",'figure-6.tiff'), transparent = False)



    # -- CHART #7 --------------------------------------------------------------
    # -- power comparison for duration of measurements

    power_unopt = []
    power_exp = []
    power_lin = []
    energy_unopt = 0
    energy_exp = 0
    energy_lin = 0

    # -- calculate power for all thre ASH
    with open(os.path.join("input",file_measurements), 'r') as f_measure:
        reader = csv.reader(f_measure)
        for row in reader:
            # -- skip comment
            if (row[0][0] != '*'):
                timest = row[0]
                dp_in = float(row[2])
                # -- momentary power
                power_unopt.append(calc._calc_power_unoptimized(timest))
                power_exp.append(calc._calc_power_exp(dp_in, timest))
                power_lin.append(calc._calc_power_lin(dp_in, timest))
                # -- energy sum
                energy_unopt += calc._calc_power_unoptimized(timest) * 0.5
                energy_exp += calc._calc_power_exp(dp_in, timest) * 0.5
                energy_lin += calc._calc_power_lin(dp_in, timest) * 0.5

    savings_exp = (energy_unopt - energy_exp) / energy_unopt * 100.0
    savings_lin = (energy_unopt - energy_lin) / energy_unopt * 100.0

    print ("\n\t*Unregulated ASH" +
           "\n\t\ttotal energy: %.1f" % energy_unopt +
           "\n\t\tsavings: %.1f" % 0.0)
    print ("\n\t*Door frame ASH" +
           "\n\t\ttotal energy: %.1f" % energy_exp +
           "\n\t\tsavings: %.1f" % savings_exp)
    print ("\n\t*Glass surface ASH" +
           "\n\t\ttotal energy: %.1f" % energy_lin +
           "\n\t\tsavings: %.1f" % savings_lin)

    # -- create figure
    fig = plt.figure(figsize = (18,10), dpi = 100);
    plt.subplots_adjust(left = padd_left, bottom = padd_bottom,
        right = padd_right, top = padd_top, wspace = None, hspace = None)
    ax1 = fig.add_subplot(111)

    # -- limits
    ax1.set_xlim(0, 192)
    ax1.set_ylim(-1, 76)

    # -- ticks
    x_ticks = np.arange(0,192,48)
    x_tickLabels = ["2016-10-23",
                    "2016-10-24",
                    "2016-10-25",
                    "2016-10-26"]
    y_ticks = np.arange(0,76,5)
    y_tickLabels = y_ticks

    ax1.set_xticks(x_ticks)
    ax1.set_xticklabels(x_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.set_yticks(y_ticks)
    ax1.set_yticklabels(y_tickLabels, fontsize = tickSize, family = 'serif')

    ax1.tick_params(axis = "both", rotation = 0, pad = 10)

    # -- labels
    ax1.set_xlabel(r'$Date$', rotation = 0,
        labelpad = x_label_padd, fontsize = labelSize, family = 'serif')
    ax1.set_ylabel(r'$P_{\rm ASH} \rm\ [W]$', rotation = 0,
        labelpad = y_label_padd, fontsize = labelSize, family = 'serif')

    # -- grid
    ax1.grid(which = "major", axis = "both", linewidth = 1, zorder = 2)

    # -- plot the data
    ax1.plot(power_exp, '-', linewidth = 2.5,
        color = _blue, label = "Door frame ASH",  zorder = 4)
    ax1.plot(power_lin, '-', linewidth=2.5,
        color = _orange, label = "Glass surface ASH",  zorder = 4)
    ax1.plot(power_unopt, '--', linewidth=2.5,
        color = _dark_grey, label = "Current state",  zorder = 4)

    # -- legend
    ax1.legend(loc = "upper right", fontsize = legendSize)

    # -- save chart
    fig.savefig(
        os.path.join("output",'figure-7.tiff'), transparent = False)

    """

    print "\n\t*...finished drawing charts.\n"



def main():
    #file_arso = "out_arso_2016.10.7-2016.11.7.csv"
    file_arso = "out_arso_2018.05.01-2018.05.31.csv"
    file_defog_ok = "defog_ok.csv"
    file_defog_fail = "defog_fail.csv"
    file_measurements = "measurements_oct_2016.csv"

    draw_charts(file_arso, file_defog_ok, file_defog_fail, file_measurements)



if __name__ == "__main__":
    main()
