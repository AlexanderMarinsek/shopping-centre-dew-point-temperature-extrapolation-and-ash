# Shopping center dew point temperature extrapolation and ASH savings calculation

Data analysis algorithm for anti-sweat heaters (ASH)  in shopping center
refrigeration technique, based on the correlation between inside and
outside dew point temperature.

Dew point data is obtained from ARSO http://www.arso.gov.si/
Calculation script predicts 30 min (0.5 h) measurement period

In order to obtain total savings, power consumption and charts,
run "python main.py" using Python 2.7
All relevant output is stored in "./output"

For further reading view the corresponding publication:
Optimizing power usage of anti-sweat heaters in glass door refrigerators
according to dew point
*link coming soon

Developed at the Faculty of Electrical Engineering, University of Ljubljana
