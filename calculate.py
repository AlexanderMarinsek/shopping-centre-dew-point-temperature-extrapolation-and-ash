# ASH savings and charts
#
#   Data analysis algorithm for anti-sweat heaters (ASH)
#   in supermarket refrigeration technique.
#
#   Dew point data is obtained from ARSO http://www.arso.gov.si/
#   Calculation script predicts 30 min (0.5 h) measurement period
#
#   In order to obtain total savings, power consumption and charts,
#   run "python main.py" using Python 2.7
#   All relevant output is stored in "./output"
#
#   For further reading view the corresponding publication:
#    Optimizing power usage of anti-sweat heaters in
#    glass door refrigerators according to dew point
#
#    *link coming soon
#
#   Developed at the Faculty of Electrical Engineering, University of Ljubljana


import os, csv, math



# -- energy savings for each ASH type / (un)optimization
class ASH_energy:

    energy_sum = 0           # -- energy consumed
    energy_sum_worst = 0     # -- worst case, measurement uncertainty is + 0.3 C
    savings = 0
    savings_worst = 0

    def __init__ (self, name):
        self.name = name

    def add_energy(self, power, power_worst):
        # -- power is added every 30min (0.5h)
        self.energy_sum += (power * 0.5)
        self.energy_sum_worst += (power_worst * 0.5)

    def calculate_savings(self, reference_sum):
        # -- savings are positive
        self.savings = (reference_sum - self.energy_sum) / reference_sum * 100.0
        self.savings_worst = (  (reference_sum - self.energy_sum_worst) /
                                reference_sum * 100.0)
        print ( "\n\t*" + self.name +
                "\n\t\ttotal energy:\t\t" + "%.1f" % self.energy_sum + " Wh" +
                "\n\t\tsavings:\t\t" + "%.1f" % self.savings + " %" +
                "\n\t\ttotal energy (worst):\t" + "%.1f" % self.energy_sum_worst
                + " Wh" +
                "\n\t\tsavings (worst):\t" + "%.1f" % self.savings_worst + " %"
                + "\n\t\tworst larger by:\t" + "%.1f" % (( 1 - self.energy_sum /
                self.energy_sum_worst) * 100) + " %")


unoptimized_ASH = ASH_energy("Unregulated ASH")
frame_ASH = ASH_energy("Door frame ASH")
glass_ASH = ASH_energy("Glass surface ASH")



# -- get parameter from timestamp
def get_date(timestamp):
    return dissect_timestamp(timestamp)[0]

def get_year(timestamp):
    return dissect_timestamp(timestamp)[1]

def get_month(timestamp):
    return dissect_timestamp(timestamp)[2]

def get_day(timestamp):
    return dissect_timestamp(timestamp)[3]

def get_time (timestamp):
    return dissect_timestamp(timestamp)[4]

def get_hour(timestamp):
    return dissect_timestamp(timestamp)[5]

def get_minute(timestamp):
    return dissect_timestamp(timestamp)[6]



# -- get parameter from timestamp
def dissect_timestamp(timestamp):
    date = timestamp.split(" ")[0]
    time = timestamp.split(" ")[1]

    year = int(date.split("-")[0])
    month = int(date.split("-")[1])
    day = int(date.split("-")[2])

    hour = int(time.split(":")[0])
    minute = int(time.split(":")[1])

    return [date, year, month, day, time, hour, minute]



# -- calculate DP
def calculate_DP(T, RH):
    A = 243.04
    B = 17.625

    # -- DP calculation independent of parameter data type
    T = float(T)
    RH = float(RH)

    # -- August-Roche-Magnus approximation
    Tdp = A*(math.log(RH/100)+(B*T/(A+T))) / (B-math.log(RH/100)-(B*T/(A+T)))

    # -- return rounded float format
    return float("{0:.1f}".format(Tdp))



# -- get unoptimized power for door frame heater
def _calc_power_unoptimized(timestamp):
    power = 55
    hour = get_hour(timestamp)

    if (hour < 6 or hour > 21):
        power *= 0.5

    return power



# -- calculate power based on dew point for door frame heater
def _calc_power_exp(DP, timestamp):
    nominal_power = 55
    # -- calculate power based on door frame heater analytical function
    if (DP < -2.59):
        power = 0
    elif (DP > -2.59 and DP <= 4.82):
        power = 0.04 * DP**2 + 0.32 * DP + 0.61
    else:
        power = 1.06 * DP**2 - 0.75 * DP - 17.8

    # -- do not exceed nominal (maximal) power
#    if (power > _calc_power_unoptimized(timestamp)):
#        power = _calc_power_unoptimized(timestamp)
    if (power > nominal_power):
        power = nominal_power

    return power



# -- calculate power based on dew point for glass surface heater
def _calc_power_lin(DP, timestamp):
    nominal_power = 55

    # -- calculate power based on glass surface heater analytical function
    # -- 0cm offset from glass edge
    #power = 1.61 * DP + 3.56
    # -- 0.5cm offset from glass edge
    #power = 1.23 * DP + 3.04
    # -- 1cm offset from glass edge
    power = 1.11 * DP + 2.23

    # -- do not exceed nominal (maximal) power
#    if (power > _calc_power_unoptimized(timestamp)):
#        power = _calc_power_unoptimized(timestamp)
    if (power > nominal_power):
        power = nominal_power

    return power



# -- load transformation: outdoor -> indoor dew point
def load_transformation():
    DP_transform = {}
    with open(os.path.join("input","transformations.csv"),'r') as f_transforms:
        transforms = csv.reader(f_transforms)
        # -- iterate all possible transformations
        for line in transforms:
            # -- skip comments
            if (line[0][0] != '*'):
                DP_out = float(line[0])
                DP_in = float(line[-1])
        		# make value - key pairs for transformation
                DP_transform[DP_out] = DP_in

    return DP_transform



# -- calculate all output line values and combine into string
# Params:
#   line_X (str): nth line
#   line_Y (str): nth+1 line
#   DP_transform (obj): object with outdoor -> indoor dew point transformations
# all variables containing _X represent the current time frame (nth), while
# all variables containing _Y represent the next time frame (nth+1)
def calculate_output (line_X, line_Y, DP_transform):
    # -- dissect line
    timestamp_X = line_X[2]
    timestamp_Y = line_Y[2]
    T_out_m_X = line_X[3]
    T_out_m_Y = line_Y[3]
    RH_out_m_X = line_X[4]
    RH_out_m_Y = line_Y[4]

    # -- calculate outdoor dew point using measured outdoor RH and temperature
    DP_out_m_X = calculate_DP(T_out_m_X, RH_out_m_X)
    DP_out_m_Y = calculate_DP(T_out_m_Y, RH_out_m_Y)
    # -- get reconstructed inside dew point from previous outdoor measurement
    DP_in_r_Y = DP_transform[DP_out_m_X]

    # -- calculate power for three heater types
    power1 = _calc_power_unoptimized(timestamp_Y)
    power2 = _calc_power_exp(DP_in_r_Y, timestamp_Y)
    power2_worst = _calc_power_exp(DP_in_r_Y + 0.3, timestamp_Y)
    power3 = _calc_power_lin(DP_in_r_Y, timestamp_Y)
    power3_worst = _calc_power_lin(DP_in_r_Y + 0.3, timestamp_Y)

    unoptimized_ASH.add_energy(power1, power1)
    frame_ASH.add_energy(power2, power2_worst)
    glass_ASH.add_energy(power3, power3_worst)

    # -- form output values list
    output_list = [
        timestamp_Y, T_out_m_Y, RH_out_m_Y, "%.2f" % DP_out_m_Y,
        "%.2f" % DP_in_r_Y, "%.2f" % power1, "%.2f" % power2, "%.2f" % power3]
    output_line = ','.join(output_list) + "\n"

    return output_line



# -- iterate input file, calculate output and save
def generate_output (input_file):
    # -- rename output file
    output_file = "out_" + "_".join(input_file.split('_')[1:])
    # -- clear output file before appending
    open(os.path.join("output", output_file), 'w').close()
    # -- load transformation: outdoor -> indoor dew point
    DP_transform = load_transformation()
    # -- comment containing output file series label names
    label_names =  "*Timestamp,T_out[C],RH_out[%],DP_out[C],DP_in[C],"\
                    "P_def[W],P_glass[W],P_frame[W]\n"

    # -- skip first line when calculating output (current and previous needed)
    skipLine = True
    # -- open input file - READ
    #with open(os.path.join("input", input_file),'r') as f_input:
    with open(os.path.join("input", input_file), 'rU') as f_input:
        # -- open output file - APPEND
        with open(os.path.join("output", output_file),'a') as f_output:
            f_output.write(label_names)
            csv_in = csv.reader(f_input)
            # -- iterate input file
            for line_in in csv_in:
                #print line_in
                # -- skip comments
                if line_in:
                    if (line_in[0][0] != '*'):
                        if not skipLine:
                            # -- get output line
                            line_out = calculate_output(
                                previous_line_in, line_in, DP_transform)
                            # -- append to output file
                            f_output.write(line_out)
                        else:
                            skipLine = False
                        # -- save previous line based on 30min (one line) offset
                        previous_line_in = line_in

    # -- calculate savings
    unoptimized_ASH.calculate_savings(unoptimized_ASH.energy_sum)
    frame_ASH.calculate_savings(unoptimized_ASH.energy_sum)
    glass_ASH.calculate_savings(unoptimized_ASH.energy_sum)



def main():
    #file_arso = "in_arso_2016.10.7-2016.11.7.csv"
    #file_arso = "in_arso_2018.04.01-2018.04.30.csv"
    file_arso = "in_arso_2018.05.01-2018.05.31.csv"
    #file_arso = "in_arso_2018.06.01-2018.06.30.csv"
    generate_output(file_arso)



if __name__ == "__main__":
    main()
